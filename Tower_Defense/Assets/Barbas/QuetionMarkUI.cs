﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuetionMarkUI : MonoBehaviour
{
    public GameObject myTips;

    public float distanceMouse;
    
    void Start()
    {
        
    }
    
    void Update()
    {
        distanceMouse = Vector2.Distance(transform.position, new Vector2(Input.mousePosition.x, Input.mousePosition.y));

        if (distanceMouse <= 40)
        {
            myTips.SetActive(true);
        }
        else if (myTips.activeSelf)
        {
            myTips.SetActive(false);
        }
    }
}
