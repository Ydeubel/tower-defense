﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingB : MonoBehaviour
{
    public GameObject gears;
    public Vector3 minCameraPos;
    public Vector3 maxCameraPos;

    void Start()
    {
        
    }
    
    void FixedUpdate()
    {
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, new Vector3(gears.transform.position.x, gears.transform.position.y, transform.position.z), 0.125f);   //Smooth Camera
        transform.position = smoothedPosition;
        transform.position = new Vector3(Mathf.Clamp(transform.position.x, minCameraPos.x, maxCameraPos.x), Mathf.Clamp(transform.position.y, minCameraPos.y, maxCameraPos.y), 
            Mathf.Clamp(transform.position.z, minCameraPos.z, maxCameraPos.z)); //Camera Bounds
    }
}
